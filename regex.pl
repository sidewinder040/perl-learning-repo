#!/usr/bin/perl -w
use strict;

# search dictionary for given pattern string

my $file = "/usr/share/dict/words";

sub search_pattern {
	my $pattern = shift;
	my $count = 0; # count number of occurences
	open FILE, $file;
	while (my $line = <FILE>) {
		if ($line =~ m/$pattern/) { 
			print $line;
			$count++;
		}
	}
	print "\nPattern Matches: $count.\n\n";
}

search_pattern @ARGV;


