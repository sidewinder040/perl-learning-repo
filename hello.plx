#!/usr/bin/perl -w

use strict;
#use diagnostics;

sub hello {
	my $name = shift;
	chomp($name);
	print "Hello $name, I hope you are well today!\n";
}
sub get_name {
	print "What is your name? ";
	my $name = <STDIN>;
}

hello get_name;
